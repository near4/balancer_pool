use crate::bconst::{Weight, BONE};
use near_sdk::Balance;

pub fn calc_spot_price(
    balance_in: Balance,
    balance_out: Balance,
    weight_in: Weight,
    weight_out: Weight,
    swap_fee: Balance,
) -> Balance {
    let numer = balance_in / weight_in;
    let denom = balance_out / weight_out;
    let ratio = numer / denom;
    let scale = BONE / (BONE - swap_fee);

    ratio * scale
}
