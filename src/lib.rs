use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::UnorderedMap;
use near_sdk::json_types::U128;
use near_sdk::{env, ext_contract, near_bindgen, AccountId, Balance, Promise};

mod bconst;
mod bmath;
mod token;

use bconst::*;
use bmath::calc_spot_price;
use token::{ext_nep21, FungibleToken, Token};

#[derive(BorshDeserialize, BorshSerialize)]
pub struct Record {
    bound: bool,
    index: u64,
    denorm: Weight,
    balance: Balance,
}

pub struct BPool {
    controller: AccountId,
    factory: AccountId,
    swap_fee: Balance,
    finalize: bool,
    public_swap: bool,
    records: UnorderedMap<AccountId, Record>,
    tokens: Vec<AccountId>,
    total_weight: Weight,
    token: Token,
}

impl Default for BPool {
    fn default() -> Self {
        panic!("BPool should be initialize before usage")
    }
}

impl BPool {
    pub fn new() -> Self {
        Self {
            controller: env::predecessor_account_id(),
            factory: env::predecessor_account_id(),
            swap_fee: MIN_FEE,
            finalize: false,
            public_swap: false,
            records: UnorderedMap::new(b"r".to_vec()),
            tokens: Vec::new(),
            total_weight: 0,
            token: Token::new(env::signer_account_id(), 0u128),
        }
    }

    pub fn isPublicSwap(&self) -> bool {
        self.public_swap
    }

    pub fn isfinalize(&self) -> bool {
        self.finalize
    }

    pub fn isBound(&self, token: AccountId) -> bool {
        self.records
            .get(&token)
            .map(|record| record.bound)
            .unwrap_or(false)
    }

    pub fn getNumTokens(&self) -> u64 {
        self.tokens.len() as u64
    }

    pub fn getCurrentTokens(&self) -> Vec<AccountId> {
        self.tokens.clone()
    }

    pub fn getFinalTokens(&self) -> Vec<AccountId> {
        assert!(self.finalize, "Err not finalized");
        self.tokens.clone()
    }

    pub fn getDenormalizeWeight(&self, token: AccountId) -> U128 {
        assert!(self.isBound(token.clone()), "Err not bound");
        self.records.get(&token).unwrap().denorm.into()
    }

    pub fn getTotalDenormalizeWeight(&self)-> U128{
        self.total_weight.into()
    }

    pub fn getNomalizeWeight(&self, token: AccountId) -> U128{
        assert!(self.isBound(token.clone()), "Err not bound");
        let denorm = self.records.get(&token).unwrap().denorm;
        (denorm/self.total_weight).into()
    }

    pub fn getBalance(&self, token: AccountId) -> U128 {
        assert!(self.isBound(token.clone()), "Err not bound");
        self.records.get(&token).unwrap().balance.into()
    }

    pub fn getSwapFee(&self)-> U128{
        self.swap_fee.into()
    }

    pub fn getController(&self)-> AccountId{
        self.controller.clone()
    }

    // setter
    pub fn setSwapFee(&mut self, swapFee: U128){
        let swap_fee = swapFee.into();
        assert!(!self.finalize, "Err: is finalized");
        assert_eq!(env::predecessor_account_id(), self.controller, "Err: Not controller");
        assert!(swap_fee >= MIN_FEE, "Err: Min Fee");
        assert!(swap_fee <= MAX_FEE, "Err: Max Fee");
        self.swap_fee = swap_fee;
    }

    pub fn setController(&mut self, controller: AccountId){
        assert_eq!(env::predecessor_account_id(), self.controller, "Err: not controller");
        self.controller = controller;
    }

    pub fn setPublicSwap(&mut self, public: bool){
        assert!(!self.finalize, "Err: is finalize");
        assert_eq!(env::predecessor_account_id(), self.controller, "Err: not controller");
        self.public_swap = public;
    }

    pub fn finalize(&mut self){
        assert_eq!(env::predecessor_account_id(), self.controller, "Err: not controller");
        assert!(!self.finalize, "Err: is finalized");
        assert!(!self.tokens.len() >= MIN_BOUND_TOKEN , "Err: Min Bound token");

        self.finalize = true;
        self.public_swap = true;

        //self.mint_pool_share();

    }
}
